//variables
const correo = document.getElementById("correo");
const clave1 = document.getElementById("contrasena");
const formulario = document.getElementById("form");
const listInsputs = document.querySelectorAll(".form-input");

//validar formulario
form.addEventListener("submit", (e) => {
    e.preventDefault();
    let condicion = validaciondatos();
    if(condicion){
        enviarFormulario();
    }
});

//funciones
function validaciondatos() {
    formulario.lastElementChild.innerHTML = "";
    let condicion = true;
    listInsputs.forEach(element => {
        element.lastElementChild.innerHTML= "";
    });
//condiciones
    if(correo.value.lenght < 1 || correo.value.trim() == "") {
        muestraError("correo", "Correo electrónico no válido*");
        condicion = false; 
    }

    if(clave1.value.lenght < 1 || clave1.value.trim() == "") {
        muestraError("contrasena", "Contraseña no válida*");
        condicion = false; 
    }
    return condicion;
}
//funcion error
function muestraError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
}
//funcion vaciar
function enviarFormulario(){
    formulario.reset();
    formulario.lastElementChild.innerHTML = "Sesion Iniciada";
}